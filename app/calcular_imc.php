<?php
$peso = $_POST['peso'];
$altura = $_POST['altura'];
$imc = $peso / ($altura ** 2);
if ($imc < 18.5) {
  $classificacao = "Abaixo do peso";
} else if ($imc >= 18.5 && $imc < 25) {
  $classificacao = "Peso normal";
} else if ($imc >= 25 && $imc < 30) {
  $classificacao = "Sobrepeso";
} else if ($imc >= 30 && $imc < 35) {
  $classificacao = "Obesidade grau 1";
} else if ($imc >= 35 && $imc < 40) {
  $classificacao = "Obesidade grau 2";
} else {
  $classificacao = "Obesidade grau 3";
}
echo "Seu IMC é: " . round($imc, 2) . "<br>";
echo "Classificação: " . $classificacao;
?>
